# README #

NOSQL/Data analytics Project - 2018

### What is this repository for? ###

This project was made for subject NOSQL and BigData Analytics on Polytechnic of Zagreb
Version 1.0

### How do I get set up? ###

For this project I was using Python 3.5 and following dependencies are needed to get it working:
pymongo
lxml
nltk
networkx
matplotlib

For faster python and dependencies setup you can use anaconda: https://anaconda.org/anaconda/python

After setup, just run projekt.py

Task 12. in project exports dataset for BayesFusion GeNIe which is free for academic purposes.
You can download GeNIe here: https://download.bayesfusion.com/files.html?category=Academia

GeNIe is also working on Linux under wine.
I have also created playonlinux script to make it run faster: https://www.playonlinux.com/en/app-3279-BayesFusion_GeNIe.html

Detailed description is documented in code (projekt.py).
