from pymongo import MongoClient
from lxml import etree as ET
from datetime import datetime
import glob
import nltk
from nltk.corpus import stopwords
from nltk.collocations import *
import networkx as nx
from operator import itemgetter
import matplotlib.pyplot as plt
from nltk.text import Text
import os
import sys


# GLOBALS

# Output everything to file output.txt for better review
# If you use Linux, you can also comment this out and use 'python projekt.py > output.txt' in shell
# sys.stdout = open('./output.txt', 'w')

opinrank_folder = 'OpinRankDataset'
my_project = '2007'

glob_pos_words = ['great', 'good', 'nice', 'lovely', 'professional', 'perfect', 'excellent']
glob_neg_words = ['bad', 'worst', 'negative']


# PROJECT FUNCTIONS

# 1.
def parse_and_insert(xml_data_file, coll):
    """
    Parsing XML file with DOC, TEXT, DATE and FAVORITE tags and inserting to Mongo database.
    :param xml_data_file: XML file which will be parsed.
    :param coll: Mongo collection where the data will be stored.
    :return: True.
    """
    xml = open(xml_data_file, 'r', encoding='ISO-8859-1').read()
    xml = "<root>" + xml + "</root>"
    parser = ET.XMLParser(recover=True)
    root = ET.fromstring(xml, parser=parser)
    car = ''

    for name in root.iter('DOCNO'):
        car = name.text

    for doc in root.iter('DOC'):
        car_data = {'car': car}
        for date in doc.iter('DATE'):
            car_data['date'] = datetime.strptime(date.text, '%m/%d/%Y').strftime('%d.%m.%Y')
        for text in doc.iter('TEXT'):
            car_data['text'] = text.text
        for fav in doc.iter('FAVORITE'):
            if fav.text:
                car_data['text'] += fav.text
        coll.insert_one(car_data)
    return True


# 2.
def word_freq_per_doc(coll_from, coll_to):
    """
    Iterating trough documents of first task, counting words and saving them to another collection.
    :param coll_from: Collection from first task which has only raw text.
    :param coll_to: Collection to save words frequency per document.
    :return: True.
    """
    for doc in coll_from.find():
        words = {}

        for word in set(doc['text'].split()):
            if word.isalpha():
                words[word] = doc['text'].count(word)

        data_new = {'car': doc['car'], 'date': doc['date'], 'words_count': words}
        coll_to.insert_one(data_new)
    return True


# 3.
def word_freq_all_doc(coll):
    """
    Returns all words frequency as the sum of all documents from task 2.
    :param coll: Collection of words frequency per document (task 2).
    :return: Dictionary of words and their frequency pairs.
    """
    words = {}
    total_words = 0
    for doc in coll.find():
        for word in doc['words_count']:
            if words.get(word):
                words[word] += doc['words_count'][word]
                total_words += doc['words_count'][word]
            else:
                words[word] = doc['words_count'][word]
                total_words += doc['words_count'][word]

    print("Total words: " + str(total_words))
    return words


# 4.
def sorted_word_list_from_dict(word_dict):
    """
    Sorting dictionary from previous task by word frequency (descending) and turning them to list.
    :param word_dict: Dictionary of words and their frequency pairs from previous task.
    :return: Sorted list of most frequent words.
    """
    return sorted(word_dict, key=word_dict.get, reverse=True)


# 5. 6.
def get_tokens(coll_search, coll_tokens=False, use_db=False):
    """
    Creating tokens(words) from all texts and returning .
    :param coll_search: Collection with text that will be tokenized.
    :param coll_tokens: Collection with already saved tokens, if exists.
    :return: List of tokens.
    """
    tokens = []
    if use_db and coll_tokens:
        if coll_tokens.count() == 0:
            for doc in coll_search.find():
                tokens += [i for i in nltk.word_tokenize(doc.get('text').replace('.', ' ').lower()) if i.isalpha()]
                coll_tokens.insert_one({'car': doc.get('car'), 'tokens': tokens})

        elif coll_tokens.count() > 0:
            for doc in coll_tokens.find():
                tokens += doc.get('tokens')

    else:
        for doc in coll_search.find():
            tokens += [i for i in nltk.word_tokenize(doc.get('text').lower()) if i.isalpha()]
    return tokens


def print_n_frequent(tokens, n):
    """
    Prints tokens by frequency using nltk library.
    :param tokens: List of tokens.
    :param n: Most frequent n tokens to print.
    :return: Stdout of tokens frequency (True).
    """
    fd = nltk.FreqDist(tokens)
    for word, frequency in fd.most_common(n):
        print(u'{};{}'.format(word, frequency))
    return True


# 8.
def freq_bigram(tokens, freq):
    """
    Creates list of most frequent bigrams (words that mostly appear together).
    :param tokens: List of tokens.
    :param freq: Number of most frequent bigrams.
    :return: List of most frequent bigrams (ngram).
    """
    finder = BigramCollocationFinder.from_words(tokens)
    finder.apply_freq_filter(freq)

    ngram = list(finder.ngram_fd.items())
    ngram.sort(key=lambda item: item[-1], reverse=True)
    return ngram


def filter_bigrams(ngram, words, other_word_type=False):
    """
    Filters ngram list by given parameters.
    :param ngram: List of ngrams (bigrams frequency).
    :param words: Word or words to be found in bigrams.
    :param other_word_type: Other word (not searched one) type (NN, JJ, VB...). No type filters by default. 
    :return: Filtered ngram list.
    """
    if not isinstance(words, list):
        words = [words]

    filtered_ngram = list(filter(lambda t: not set(words).isdisjoint(t[0]), ngram))

    if other_word_type:
        filtered_ngram_type = []
        for tup in filtered_ngram:
            for w in tup[0]:
                if w not in words and nltk.pos_tag([w])[0][1] == other_word_type:
                    filtered_ngram_type.append(tup)

        filtered_ngram = list(filtered_ngram_type)

    return filtered_ngram


# 9.
def draw_sna_network(ngram, print_net_info=False):
    """
    Draws SNA network based on ngram from previous task.
    :param ngram: List of most frequent bigrams.
    :param print_net_info: Boolean value, if true, the additional network info will be printed out.
    :return: Drawable plot of SNA network (True).
    """
    G = nx.Graph()

    node_sizes = {}
    for bg in ngram:
        G.add_edge(bg[0][0], bg[0][1])

        if not node_sizes.get(bg[0][0]):
            node_sizes[bg[0][0]] = bg[1] / 10
        else:
            node_sizes[bg[0][0]] += bg[1] / 10

        if not node_sizes.get(bg[0][1]):
            node_sizes[bg[0][1]] = bg[1] / 10
        else:
            node_sizes[bg[0][1]] += bg[1] / 10

    pos = nx.fruchterman_reingold_layout(G)
    sizes = []
    for item in node_sizes:
        sizes.append(node_sizes[item])

    if print_net_info:
        print("Info:")
        print(nx.info(G))
        print("Degree histogram:")
        print(nx.degree_histogram(G))
        print("Density :")
        print(nx.density(G))
        print("Number of nodes :")
        print(G.number_of_nodes())
        print("Number of edges :")
        print(G.number_of_edges())
        dc = nx.degree_centrality(G)

        sorted_degree = sorted(dc.items(), key=itemgetter(1), reverse=True)
        print("Sorted degree :")
        print(sorted_degree[0:5])

        bc = nx.betweenness_centrality(G)
        sorted_betweenness = sorted(bc.items(), key=itemgetter(1), reverse=True)
        print("Sorted betweenness :")
        print(sorted_betweenness[0:5])

        cc = nx.closeness_centrality(G)
        sorted_closeness = sorted(cc.items(), key=itemgetter(1), reverse=True)
        print("Sorted closeness :")
        print(sorted_closeness[0:5])
    nx.draw_networkx(G, pos, node_size=sizes)
    plt.show()
    return True


# 12.
def nltk_bayes_prediction(coll):
    """
    Creates bayes network (NLTK) of most frequent words in collection text. 
    :param coll: Collection of documents with text.
    :return: Predictions info (True).
    """
    stop = set(stopwords.words('english'))
    cars = [car for car in coll.distinct('car')]
    training_set = []

    for i, car in enumerate(cars):
        pos_words = ['great', 'good', 'nice', 'lovely', 'professional', 'perfect']
        neg_words = ['bad', 'worst', 'negative']
        all_words = []

        for doc in coll.find({'car': car}):
            for word in doc.get('text').lower().split():
                if word.isalpha() and word not in stop:
                    all_words.append(word)

        most_common_words = set([a[0] for a in nltk.FreqDist(all_words).most_common(300)])
        least_common_words = set([a[0] for a in nltk.FreqDist(all_words).most_common(600)][-300:])

        idx = nltk.text.ContextIndex(all_words)
        pos_words += idx.similar_words("good")
        neg_words += idx.similar_words("bad")

        def get_train_set(words):
            train_set = []
            pos_dict = {}
            neg_dict = {}
            for word in words:
                if word in pos_words:
                    pos_dict[word] = True
                elif word in neg_words:
                    neg_dict[word] = True
                else:
                    pos_dict[word] = False
                    neg_dict[word] = False

            train_set.append((pos_dict, 'pos'))
            train_set.append((neg_dict, 'neg'))
            return train_set

        if i < 5:
            training_set += get_train_set(most_common_words)
        testing_set = get_train_set(least_common_words)

        classifier = nltk.NaiveBayesClassifier.train(training_set)
        print("Good car percentage:", (nltk.classify.accuracy(classifier, testing_set)) * 100)
        classifier.show_most_informative_features(15)
    return True


# 12.
def create_genie_data_set(coll, directory):
    """
    Creates datasets for importing to BayesFusion GeNIe software.
    Datasets contain 'good' or 'bad' words which are used for prediction whenever a car is good or bad.
    Lists of fixed good and bad words already exist. Iterating trough them and finding similar words in texts, the
    lists are extending and at the end th
    :param coll: Collection of documents with texts.
    :param directory: Directory to save dataset files.
    :return: True
    """
    stop = set(stopwords.words('english'))
    cars = [car for car in coll.distinct('car')]
    brands = [b.split('_')[1] for b in cars]

    custom_pos_words = glob_pos_words
    custom_neg_words = glob_neg_words

    cars_dir = directory + 'cars/'
    if not os.path.exists(cars_dir):
        os.makedirs(cars_dir)

    brands_dir = directory + 'brands/'
    if not os.path.exists(brands_dir):
        os.makedirs(brands_dir)

    merged_dataset_file = open(directory + "merged_all_dataset.txt", "w")
    merged_dataset_text = 'Words Car\n'

    for brand in brands:
        brand_dataset_loc = brands_dir + brand + ".txt"
        brand_dataset_file = open(brand_dataset_loc, "w")
        merged_brand_text = 'Words Car\n'
        for car in cars:
            if brand in car.split('_'):
                all_words = []
                pos_words = list(custom_pos_words)
                neg_words = list(custom_neg_words)

                car_dataset_loc = cars_dir + car + ".txt"
                car_dataset_file = open(car_dataset_loc, "w")
                file_text = "Words Car\n"

                for doc in coll.find({'car': car}):
                    all_words += [a for a in doc.get('text').lower().split() if a.isalpha() and a not in stop]

                idx = nltk.text.ContextIndex(all_words)

                for word in custom_pos_words:
                    pos_words += idx.similar_words(word)

                for word in custom_neg_words:
                    neg_words += idx.similar_words(word)

                for word in set(pos_words):
                    for i in range(all_words.count(word)):
                        file_text += word + " good\n"
                        merged_dataset_text += word + " good\n"
                        merged_brand_text += word + " good\n"

                for word in set(neg_words):
                    for i in range(all_words.count(word)):
                        file_text += word + " bad\n"
                        merged_dataset_text += word + " bad\n"
                        merged_brand_text += word + " bad\n"

                car_dataset_file.write(file_text)
        brand_dataset_file.write(merged_brand_text)
    merged_dataset_file.write(merged_dataset_text)
    return True


def create_genie_ngram_data_set(coll, directory, freq=50):
    stop = set(stopwords.words('english'))
    cars = [car for car in coll.distinct('car')]

    custom_pos_words = glob_pos_words
    custom_neg_words = glob_neg_words

    merged_dataset_text = 'Words Car\n'

    for car in cars:
        all_words = []
        pos_words = list(custom_pos_words)
        neg_words = list(custom_neg_words)

        car_dataset_loc = directory + car + ".txt"
        car_dataset_file = open(car_dataset_loc, "w")
        file_text = "Words Car\n"

        for doc in coll.find({'car': car}):
            all_words += [a for a in nltk.word_tokenize(doc.get('text').lower()) if a.isalpha() and a not in stop]

        idx = nltk.text.ContextIndex(all_words)

        for word in custom_pos_words:
            pos_words += idx.similar_words(word)

        for word in custom_neg_words:
            neg_words += idx.similar_words(word)

        bigrams = freq_bigram(all_words, freq)
        bigrams_filtered_pos = filter_bigrams(bigrams, pos_words)
        bigrams_filtered_neg = filter_bigrams(bigrams, neg_words)

        # Words by every car
        for t in bigrams_filtered_pos:
            word = t[0][0] + "_" + t[0][1]
            for i in range(t[1]):
                file_text += word + " good\n"
                merged_dataset_text += word + " good\n"

        for t in bigrams_filtered_neg:
            word = t[0][0] + "_" + t[0][1]
            for i in range(t[1]):
                file_text += word + " bad\n"
                merged_dataset_text += word + " bad\n"

        if not bigrams_filtered_pos:
            file_text += "good good\n"
            merged_dataset_text += "good good\n"

        if not bigrams_filtered_neg:
            file_text += "bad bad\n"
            merged_dataset_text += "bad bad\n"

        car_dataset_file.write(file_text)

    merged_dataset_file = open(directory + "merged_all_dataset.txt", "w")
    merged_dataset_file.write(merged_dataset_text)
    return True


if __name__ == '__main__':
    print("Starting application. Opening MongoDB connections.")
    client = MongoClient()

    dbnames = client.database_names()
    if 'baza' in dbnames:
        client.drop_database('baza')

    db = client.baza
    coll_cars = db.cars
    coll_cars_freq = db.cars_freq
    coll_cars_all_freq = db.cars_all_freq

    print("1.task - Parsing XML-s and inputing to MongoDB")
    if coll_cars.count() == 0:
        for filename in glob.iglob(opinrank_folder + '/cars/*/' + my_project + '*', recursive=True):
            parse_and_insert(filename, coll_cars)
    else:
        print("Collection already exists. Using existing one.")

    print("\n2.task - Creating documents by words frequency")
    if coll_cars_freq.count() == 0:
        word_freq_per_doc(coll_cars, coll_cars_freq)
    else:
        print("Collection already exists. Using existing one.")

    print("\n3.task - Words from all documents by frequency")
    all_doc_word_freq = {}
    if coll_cars_all_freq.count() == 0:
        all_doc_word_freq = word_freq_all_doc(coll_cars_freq)
        coll_cars_all_freq.insert_one({'all_words': all_doc_word_freq})
    else:
        print("Collection already exists. Using existing one.")
        for doc in coll_cars_all_freq.find():
            all_doc_word_freq = doc.get('all_words')
    print(all_doc_word_freq)

    print("\n4.task - Sorted words by frequency to list")
    print(sorted_word_list_from_dict(all_doc_word_freq))

    print("\n5.task - NLTK word frequency")
    tokens = get_tokens(coll_cars)
    print_n_frequent(tokens, 30)

    print("\n6.task - Removing stopwords")
    print("Need to download packages with nltk.download('punkt') and nltk.download('stopwords') before running this.")
    try:
        nltk.data.find('tokenizers/punkt')
    except LookupError:
        nltk.download('punkt')

    try:
        nltk.data.find('corpora/stopwords')
    except LookupError:
        nltk.download('stopwords')

    coll_filtered_tokens = db.filtered_tokens
    filtered_tokens = []
    stop = set(stopwords.words('english'))

    if coll_filtered_tokens.count() == 0:
        filtered_tokens = [tok for tok in tokens if tok not in stop]

        while sys.getsizeof(filtered_tokens) > 7600000:
            del filtered_tokens[-1]

        coll_filtered_tokens.insert_one({'filtered_tokens': filtered_tokens})

    else:
        for doc in coll_filtered_tokens.find():
            filtered_tokens = doc['filtered_tokens']

    print_n_frequent(filtered_tokens, 30)

    print("\n7.task - Word frequency graph")
    fd = nltk.FreqDist(filtered_tokens)
    fd.plot(30, cumulative=False)

    print("\n8.task - Creating bigrams")
    ngram = freq_bigram(filtered_tokens, 100)
    print(ngram)

    print(filter_bigrams(ngram, glob_pos_words))

    print("\n9.task - SNA graph")
    draw_sna_network(ngram)

    print("\n10.task - Concordance by most frequent n words")
    fd = nltk.FreqDist(filtered_tokens)
    for word, frequency in fd.most_common(10):
        print(Text(tokens).concordance(word))

    print("\n11.task - Linguistic classification")
    print("Need to run nltk.download('averaged_perceptron_tagger')")
    try:
        nltk.data.find('taggers/averaged_perceptron_tagger')
    except LookupError:
        nltk.download('averaged_perceptron_tagger')
    [print(a) for a in nltk.tag.pos_tag(set(tokens))]

    print("\n12.task - Bayes network and datasets")
    # nltk_bayes_prediction(coll_cars)

    genie_data_set_dir = "./genie_datasets/"
    if not os.path.exists(genie_data_set_dir):
        os.makedirs(genie_data_set_dir)

    create_genie_data_set(coll_cars, genie_data_set_dir)

    genie_data_set_ngram_dir = "./genie_ngram_datasets/"
    if not os.path.exists(genie_data_set_ngram_dir):
        os.makedirs(genie_data_set_ngram_dir)

    create_genie_ngram_data_set(coll_cars, genie_data_set_ngram_dir, 10)
