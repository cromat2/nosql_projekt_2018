from bs4 import BeautifulSoup as bs

file = open("2007_volvo_c70", "r").read()

header = bs(file, "lxml")
print(header)
car = {}

for docno in header.find_all("docno"):
    car_name = docno

car["name"] = car_name.text
car["data_array"] = []

for doc in header.find_all("doc"):
    car_data = {}

    for date in doc.find_all("date"):
        car_data["date"] = date.text

    for text in doc.find_all("text"):
        car_data["text"] = text.text

    for favorite in doc.find_all("favorite"):
        car_data["favorite"] = favorite.text

    car["data_array"].append(car_data)

print(car)
